//
//  Credits.swift
//  game
//
//  Created by Silvio Fosso on 23/11/2019.
//  Copyright © 2019 Silvio Fosso. All rights reserved.
//

import Foundation
import SpriteKit

class Crediti : SKScene {
    override func didMove(to view: SKView) {
        if let c = self.childNode(withName: "Back")
        {
            c.zPosition = 2
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let node : SKNode = self.atPoint(location)
            if node.name == "Back" {
                let scene = GameScene(fileNamed: "Menu")!
                scene.scaleMode = .aspectFill
                let transition = SKTransition.moveIn(with: .right, duration: 1)
                self.view?.presentScene(scene, transition: transition)
            }
            
        }
    }
    
}
