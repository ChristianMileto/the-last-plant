//
//  Menu.swift
//  game
//
//  Created by Silvio Fosso on 22/11/2019.
//  Copyright © 2019 Silvio Fosso. All rights reserved.
//

import Foundation
import GameplayKit
import CoreMotion
import AVFoundation
class Menu: SKScene,SKPhysicsContactDelegate {
let btn = SKSpriteNode()
    var bombSoundEffect: AVAudioPlayer?
    override func didMove(to view: SKView) {
            
          let path = Bundle.main.path(forResource: "M.mp3", ofType:nil)!
          let url = URL(fileURLWithPath: path)

          do {
            if(Messo == false)
            {
              audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.play()
            Messo = true
            }
          } catch {
              // couldn't load file :(
          }
    }
    
    
    
    var audioPlayer : AVAudioPlayer? {
        get {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            return appDelegate.audioPlayer
        }
        set {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.audioPlayer = newValue
        }
    }
    
    var Messo : Bool {
           get {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
            return appDelegate.Messo ?? false
           }
           set {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.Messo = newValue
           }
       }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let node : SKNode = self.atPoint(location)
            if node.name == "btn" {
                let scene = GameScene(fileNamed: "Prologo")!
                scene.scaleMode = .aspectFill
                let transition = SKTransition.moveIn(with: .right, duration: 1)
                self.view?.presentScene(scene, transition: transition)
            }
            if node.name == "Credits" {
                let scene = GameScene(fileNamed: "Credits")!
                scene.scaleMode = .aspectFill
                let transition = SKTransition.moveIn(with: .right, duration: 1)
                self.view?.presentScene(scene, transition: transition)
            }
        }
    }

}
